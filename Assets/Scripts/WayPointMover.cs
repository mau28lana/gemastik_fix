using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WayPointMover : MonoBehaviour
{
    [SerializeField] private WayPoint waypoints;
    [SerializeField] private float moveSpeed = 5f;
    [SerializeField] private float distanceTreshold = 0.1f;
    [SerializeField] private float safeDistance = 2f;
    private Transform currentWaypoint;

    void Start()
    {   
        RaycastHit hit;
        Physics.Raycast(transform.position,transform.forward,out hit,safeDistance);
        if (hit.transform)
        {
            if(hit.transform.tag == "StopWall")
            {
                Stop();
            }
            if(hit.transform.tag == "Car")
            {
                Stop();
            }
        }

        else
        {
            Move();
        }
        
            // for(int i = 0; i < waypoints.Length; i++)
            // {
            //     currentWaypoint = waypoints[i].GetNextWaypoint(currentWaypoint);
            //     transform.position = currentWaypoint.position;

            //     currentWaypoint = waypoints[i].GetNextWaypoint(currentWaypoint);
            //     transform.LookAt(currentWaypoint);
            // }
    }

    void Update()
    {   
        RaycastHit hit;
        Physics.Raycast(transform.position,transform.forward,out hit,safeDistance);
        if (hit.transform)
        {
            if(hit.transform.tag == "StopWall")
            {
                Stop();
            }
            if(hit.transform.tag == "Car")
            {
                Stop();
            }
        }

        else
        {
            ContinousMove();
        }
        
        // for(int i = 0; i < waypoints.Length; i++)
        // {
        //     transform.position = Vector3.MoveTowards(transform.position, currentWaypoint.position, moveSpeed * Time.deltaTime);
        //     if(Vector3.Distance(transform.position, currentWaypoint.position) < distanceTreshold)
        //     {
                
        //         currentWaypoint = waypoints[i].GetNextWaypoint(currentWaypoint);
        //         transform.LookAt(currentWaypoint);
        //     }
        // }
    }
    void Stop()
    {
        transform.position += new Vector3(0, 0, 0);
    }

    void ContinousMove()
    {
        transform.position = Vector3.MoveTowards(transform.position, currentWaypoint.position, moveSpeed * Time.deltaTime);
        if(Vector3.Distance(transform.position, currentWaypoint.position) < distanceTreshold)
        {   
            currentWaypoint = waypoints.GetNextWaypoint(currentWaypoint);
            transform.LookAt(currentWaypoint);
        }
    }
    
    void Move()
    {
        currentWaypoint = waypoints.GetNextWaypoint(currentWaypoint);

            transform.position = currentWaypoint.position;
            currentWaypoint = waypoints.GetNextWaypoint(currentWaypoint);
            transform.LookAt(currentWaypoint);
    }
}
