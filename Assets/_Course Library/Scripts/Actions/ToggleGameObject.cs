using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleGameObject : MonoBehaviour
{
    private GameObject gameObject = null;

    private void Awake()
    {
        gameObject = GetComponent<GameObject>();
    }

    public void Toggle()
    {
        bool isActive = !gameObject.activeSelf;
        gameObject.SetActive(!isActive);
    }
}
