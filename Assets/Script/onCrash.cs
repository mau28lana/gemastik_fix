using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class onCrash : MonoBehaviour
{
    Collider coll;

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        
        
            if(hit.collider.gameObject.name == "SchoolBus")
            {
                Debug.Log(hit.collider.name+" has my fantastic tag name!");
            }
        
    }

    void PlayerCrash()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
