using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car_AI : MonoBehaviour
{
    public float safeDistance = 2f;

    public float carSpeed = 5f;

    private void Update()
    {
        RaycastHit hit;
        Physics.Raycast(transform.position,transform.forward,out hit,safeDistance);
        if (hit.transform)
        {
            if(hit.transform.tag == "StopWall")
            {
                Stop();
            }
            if(hit.transform.tag == "Car")
            {
                Stop();
            }
        }

        else if(this.transform.rotation.y > 0)
        {
            Move1();
        }

        else
        {
            Move2();
        }
        
    }
    void OnDrawGizmosSelected()
    {
        // Draws a 5 unit long red line in front of the object
        Gizmos.color = Color.red;
        Vector3 direction = transform.TransformDirection(Vector3.forward) * safeDistance;
        Gizmos.DrawRay(transform.position, direction);
    }

    void Stop()
    {
        transform.position += new Vector3(0, 0, 0);
    }

    void Move1()
    {
        transform.position += new Vector3(carSpeed * Time.deltaTime, 0, 0);
    }
    void Move2()
    {
        transform.position -= new Vector3(carSpeed * Time.deltaTime, 0, 0);
    }
}
