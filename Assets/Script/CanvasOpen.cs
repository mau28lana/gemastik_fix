using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasOpen : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        gameObject.SetActive(true);
    }

    private void OnTriggerExit(Collider other)
    {
        gameObject.SetActive(false);
    }
}
